package ShopListPackage;

/**
 * Created by ras on 02.02.2018.
 */
public class ProductAmountDiscount extends Product{
    private float priceDiscount;
    private int minimalAmountDiscount;

    // конструктор
    public ProductAmountDiscount() {
        super();
        setPriceDiscount(0);
        setMinimalAmountDiscount(2);
    }

    public ProductAmountDiscount(float priceDiscount, int minimalAmountDiscount) {
        super();
        setPriceDiscount(priceDiscount);
        setMinimalAmountDiscount(minimalAmountDiscount);
    }

    public ProductAmountDiscount(String productName, double productPrice, int productAmount, float priceDiscount , int minimalAmountDiscount) {
        super(productName, productPrice, productAmount);
        setPriceDiscount(priceDiscount);
        setMinimalAmountDiscount(minimalAmountDiscount);
    }

    // установки считывания полей
    public void setPriceDiscount(float priceDiscount) {
        if ((priceDiscount <= 100) && (priceDiscount >= 0)) {
            this.priceDiscount = priceDiscount;
        } else {
            throw new IllegalArgumentException();
        }

    }

    public float getPriceDiscount() {
        return priceDiscount;
    }

    public void setMinimalAmountDiscount(int minimalAmountDiscount) {
        if (minimalAmountDiscount > 1) {
            this.minimalAmountDiscount = minimalAmountDiscount;
        } else {
            throw new IllegalArgumentException();
        }

    }

    public float getMinimalAmountDiscount() {
        return minimalAmountDiscount;
    }

    @Override
    public double getCost() {
        if(super.getProductAmount() < getMinimalAmountDiscount()) {
            return super.getCost();
        }
        return super.getCost() * getPriceDiscount() / 100;
    }

    @Override
    public String toString() {
        return super.toString() + getPriceDiscount() + ";\t" + getMinimalAmountDiscount() + ";\t";
    }

}

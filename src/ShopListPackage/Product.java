package ShopListPackage;

/**
 * Created by ras on 02.02.2018.
 */
public class Product {
    private String productName;
    private double productPrice;
    private int productAmount;

    // конструкторы
    public Product() {
        productName = "";
        productPrice = 0;
        productAmount = 0;
    }

    public Product(String productName) {
        this();
        setProductName(productName);
    }

    public Product(double productPrice) {
        this();
        setProductPrice(productPrice);
    }

    public Product(int productAmount) {
        this();
        setProductAmount(productAmount);
    }

    public Product(String productName, double productPrice, int productAmount) {
        setProductName(productName);
        setProductPrice(productPrice);
        setProductAmount(productAmount);
    }

    // установки считывания полей
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductPrice(double productPrice) {
        if(productPrice < 0) {
            throw new IllegalArgumentException();
        } else {
            this.productPrice = productPrice;
        }
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductAmount(int productAmount) {
        if(productAmount < 0) {
            throw new IllegalArgumentException();
        } else {
            this.productAmount = productAmount;
        }
    }

    public int getProductAmount() {
        return productAmount;
    }

    public double getCost() {
        return getProductPrice()*getProductAmount();
    }

    @Override
    public String toString() {
        return (getProductName() + ";\t" + getProductPrice() + ";\t" + getCost() + ";\t" + getProductAmount()) + ";\t";
    }

    public boolean equals(Product product) {
        if((this.getProductName() == product.getProductName()) && (this.getProductPrice() == product.getProductPrice())){
            return true;
        }
        return false;
    }
}

package ShopListPackage;

/**
 * Created by ras on 02.02.2018.
 */
public class ProductPriceDiscount extends Product {
    private float priceDiscount;

    // конструктор
    public ProductPriceDiscount() {
        super();
        setPriceDiscount(0);
    }

    public ProductPriceDiscount(float priceDiscount) {
        super();
        setPriceDiscount(priceDiscount);
    }

    public ProductPriceDiscount(String productName, double productPrice, int productAmount, float priceDiscount) {
        super(productName, productPrice, productAmount);
        setPriceDiscount(priceDiscount);
    }

    // установки считывания полей
    public void setPriceDiscount(float priceDiscount) {
        if ((priceDiscount <= 100) && (priceDiscount >= 0)) {
            this.priceDiscount = priceDiscount;
        } else {
            throw new IllegalArgumentException();
        }

    }

    public float getPriceDiscount() {
        return priceDiscount;
    }

    @Override
    public double getCost() {
        return super.getCost() * getPriceDiscount() / 100;
    }

    @Override
    public String toString() {
        return super.toString() + getPriceDiscount() + ";\t";
    }
}

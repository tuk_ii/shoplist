import ShopListPackage.Product;
import ShopListPackage.ProductAmountDiscount;
import ShopListPackage.ProductPriceDiscount;

/**
 * Created by ras on 02.02.2018.
 */
public class ShopRunner {
    public static void main(String[] args) {
        Product[] product = new Product[6];

        for (int index = 0; index < 2; index++) {
            product[index] = new Product("Milk " + (index + 1), 8.5 + index, index);
        }

        for (int index = 2; index < 4; index++) {
            product[index] = new ProductPriceDiscount("Bread", 10, index + 1, 10);
        }

        for (int index = 4; index < 6; index++) {
            product[index] = new ProductAmountDiscount("Vodka", 42.3, index + 1, 50, 6);
        }

        double maxPrice = 0;
        int maxIndex = 0;
        Product patternProduct = product[0];
        byte equalsFlag = 0;

        for (int index = 0; index < product.length; index++) {
            System.out.println(product[index].toString());
            if (product[index].getCost() > maxPrice) {
                maxPrice = product[index].getCost();
                maxIndex = index;
            }
            if (product[index].equals(patternProduct)) {
            } else {
                equalsFlag++;
            }
        }
        System.out.print("\n");
        System.out.println(product[maxIndex].toString());

        if(equalsFlag>0){
            System.out.println("Products are different");
        } else {
            System.out.println("Products are the same");
        }
    }
}
